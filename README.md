# python-flask-docker + CI/CD on Gitlab and EC2
Basic Python Flask app in Docker which prints the hostname and IP of the container

### Build application
Build the Docker image manually by cloning the Git repo.
```
$ git clone https://gitlab.com/tanel06/gitlabci-ec2-deployment.git
$ docker build -t tanel06/flask .
```


### Run the container
Create a container from the image.
```
$ docker run --name my-container -d -p 8080:8080 tanel06/flask
```

Now visit http://localhost:8080 



# creattion de Pipeline CI-CD  Gitlab-EC2
  ## Create an   EC2 instance on AWS
  * Key-pair creation : ```$ aws ec2 create-key-pair --key-name flask --query 'KeyMaterial' --output text > MyKeyPair.pem```
  * create security group : ```$ aws ec2 create-security-group --group-name my-sg --description "My security group"```
  ```sh
  {
    "GroupId": "sg-903004f8"
  }
  ```
  * Added authorization rules on the flask group (ports 22 and 8080): 
  ```sh
  aws ec2 authorize-security-group-ingress --group-id sg-903004f8 --protocol tcp --port 22 --cidr 0.0.0.0/0
  aws ec2 authorize-security-group-ingress --group-id sg-903004f8 --protocol tcp --port 8080 --cidr 0.0.0.0/0
  ```
  * Launching  Amazon EC2 instances :
  ```sh
  aws ec2 run-instances --image-id ami-0de12f76efe134f2f --instance-type t2.micro --security-group-ids sg-903004f8 --key-name flask --profile ec2-user

  ```

  ## Connect to your EC2 and install Docker:
  ```sh
  sudo yum update -y
  sudo amazon-linux-extras install docker
  sudo service docker start
  sudo usermod -a -G docker ec2-user
  ```
  
  ## Use or check the gitlab-ci.yml file like in the repo
  * gitlab-ci.yml file
```sh
#Pour faire du docker in docker 
image: gitlab/dind 

stages:
    - build
    - test
    - deploy
  
  
build_job:
  stage: build
  script:
    - docker build -t tanel06/flask:${CI_PIPELINE_IID} .
    - docker run -d -p 8080:8080 tanel06/flask
    - docker ps
    - docker login -u ${DOCKER_LOGIN} -p ${DOCKER_PWD}
    - docker push tanel06/flask:${CI_PIPELINE_IID}
    
        
test_job_aws:
  stage: test
  script:
    - bash ./scripts/deploy.sh #
    
```

**_Note_** : DOCKER_LOGIN, DOCKER_PASSWORD, AWS_IP, AWS_KEY, AWS_PEM must be defined in the Gitlab CI/CD environment variables.

* Création of deploys.sh script:
```sh
#!/bin/bash
set -e
mkdir -p ~/.ssh  # On crée le dossier SSH
touch ~/.ssh/id_rsa  # fichier qui contiendra la clé
echo - e "$AWS_PEM" >  ~/.ssh/id_rsa # On insère la clé dans le fichier
chmod 600 ~/.ssh/id_rsa

# on change la config de ssh
touch ~/.ssh/congig
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config # On désactive le Key checking

#conect en ssh
ssh ec2-user@$AWS_IP 'bash -s' < ./scripts/update-restart.sh
```
* Création of update-restart.sh script:
```sh
#!/bin/bash
set -e
​
if [ "$(docker ps -q -f name=flask)" ]; then
    docker rm -f flask
    docker rmi -f $(docker images -q)
fi
​
    docker run -p 8080:8080 -d --name flask tanel/flask
```


