#!/bin/bash
set -e
mkdir -p ~/.ssh  # On crée le dossier SSH
touch ~/.ssh/id_rsa  # fichier qui contiendra la clé
echo - e "$AWS_PEM" >  ~/.ssh/id_rsa # On insère la clé dans le fichier
chmod 600 ~/.ssh/id_rsa

# on change la config de ssh
touch ~/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config # On désactive le Key checking

#conect en ssh
ssh ec2-user@$AWS_IP 'bash -s' < ./scripts/update-restart.sh


