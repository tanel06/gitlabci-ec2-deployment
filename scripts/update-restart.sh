#!/bin/bash
set -e

​
if [ "$(docker ps -q -f name=flask)" ]; then
    docker rm -f flask
    docker rmi -f $(docker images -q)
fi
​
    docker run -p 8080:8080 -d --name flask tanel/flask
