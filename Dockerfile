FROM python:3.6
WORKDIR /usr/src/app
COPY . .
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 8282
CMD ["python", "app/app.py"]